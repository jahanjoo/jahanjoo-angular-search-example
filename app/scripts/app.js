'use strict';

angular

/**
 * @ngdoc overview
 * @name jahanjooAngularSearchExampleApp
 * @description # jahanjooAngularSearchExampleApp
 * 
 * Main module of the application.
 */
.module('jahanjooAngularSearchExampleApp', [ 'ngRoute', 'pluf.jahanjoo' ]);
