'use strict';
angular.module('jahanjooAngularSearchExampleApp')

/**
 * @ngdoc controller
 * @name SearchCtrl
 * @memberof jahanjooAngularSearchExampleApp
 * @description # جستجو مکان‌ها
 * 
 * این کنترلر ابزارهای مورد نیاز در نمایش جستجوی مکان‌ها را ایجاد می‌کند.
 */
.controller('SearchCtrl', function($scope, $jahanjoo, PaginatorParameter) {
	var paginatorParameter = new PaginatorParameter();

	function search(point) {
		paginatorParameter.put('latitude', point.latitude);
		paginatorParameter.put('longitude', point.longitude);
		return $jahanjoo.locations(paginatorParameter)//
		.then(function(locations) {
			$scope.locations = locations;
		});
	}

	$scope.search = search;
	$scope.locations = null;
});
