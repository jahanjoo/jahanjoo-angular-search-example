'use strict';

angular.module('jahanjooAngularSearchExampleApp')

/**
 * مسیریابی و حالت‌های سیستم را تعیین می‌کند
 * 
 * تمام حالت‌ها و نمایش‌های سیستم توی این تنظیم‌ها ایجاد شده .
 */
.config(function($routeProvider) {
	$routeProvider//
	.when('/', {
		templateUrl : 'views/search.html',
		controller : 'SearchCtrl',
	})//
	.when('/about', {
		templateUrl : 'views/about.html',
	})//
	.when('/tutorial', {
		templateUrl : 'views/tutorial.html',
	})//
	.otherwise({
		redirectTo : '/'
	});
});